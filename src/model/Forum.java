package model;

import database.RequestManager;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Forum extends BaseModel{

    private int ID_CREATOR;
    private int ID_FORUM;
    private String TITLE;
    private String DESCRPITION;

    public Forum(int ID_CREATOR, int ID_FORUM, String TITLE, String DESCRPITION) {
        this.ID_CREATOR = ID_CREATOR;
        this.ID_FORUM = ID_FORUM;
        this.TITLE = TITLE;
        this.DESCRPITION = DESCRPITION;
    }

    public Forum() {
    }

    public static Forum getForumById(int ID_FORUM) throws SQLException {
        ResultSet resultSet = requestManager.doSelect("select * from FORUM " +
                "where ID_FORUM = " + ID_FORUM);
        if (resultSet != null) {
            if (!resultSet.isBeforeFirst()) {
                System.out.println("No forum with this ID");
            } else {
                while (resultSet.next()) {
                    return new Forum(
                            resultSet.getInt("ID_CREATOR"),
                            resultSet.getInt("ID_FORUM"),
                            resultSet.getString("TITLE"),
                            resultSet.getString("DESCRIPTION")
                    );
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "Forum{" +
                "ID_CREATOR=" + ID_CREATOR +
                ", ID_FORUM=" + ID_FORUM +
                ", TITLE='" + TITLE + '\'' +
                ", DESCRPITION='" + DESCRPITION + '\'' +
                '}';
    }
}
