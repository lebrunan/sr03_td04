package model;

import java.util.Date;

public class UserForum {
    private int ID_USER;
    private int ID_FORUM;
    private Date JOINING_DATE;

    public UserForum(int ID_USER, int ID_FORUM, Date JOINING_DATE) {
        this.ID_USER = ID_USER;
        this.ID_FORUM = ID_FORUM;
        this.JOINING_DATE = JOINING_DATE;
    }
}
