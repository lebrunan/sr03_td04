package model;

import java.util.Date;

public class User {

    private int ID_USER;
    private String NAME;
    private String FIRSTNAME;
    private Date CREATION_DATE;
    private String PASSWORD;
    private String EMAIL;
    private int Is_ADMIN;

    public User(int ID_USER, String NAME, String FIRSTNAME, Date CREATION_DATE, String PASSWORD, String EMAIL, int is_ADMIN) {
        this.ID_USER = ID_USER;
        this.NAME = NAME;
        this.FIRSTNAME = FIRSTNAME;
        this.CREATION_DATE = CREATION_DATE;
        this.PASSWORD = PASSWORD;
        this.EMAIL = EMAIL;
        Is_ADMIN = is_ADMIN;
    }
}
