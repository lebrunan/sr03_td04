package model;

import java.util.Date;

public class Message {
    private int ID_MESSAGE;
    private int ID_FORUM;
    private int ID_SENDER;
    private String CONTENT;
    private Date PUBLISH_DATE;

    public Message(int ID_MESSAGE, int ID_FORUM, int ID_SENDER, String CONTENT, Date PUBLISH_DATE) {
        this.ID_MESSAGE = ID_MESSAGE;
        this.ID_FORUM = ID_FORUM;
        this.ID_SENDER = ID_SENDER;
        this.CONTENT = CONTENT;
        this.PUBLISH_DATE = PUBLISH_DATE;
    }
}
