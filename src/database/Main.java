package database;

import model.Forum;
import java.sql.SQLException;

public class Main {

    public static void main(String[] args) throws SQLException {

        Forum forum1 = Forum.getForumById(1);
        if (forum1 == null) {
            System.out.println("No forum with this name");
        } else {
            System.out.println(forum1.toString());
        }
    }
}
