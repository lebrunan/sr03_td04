package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class RequestManager {

    Connection connection;

    public RequestManager() {
        try {
            connection = DatabaseConnection.getInstance().getConnection();
        } catch (SQLException e) {
            System.out.println("cannot connect database.");
            e.printStackTrace();
        }
    }

    public void doUpdate(String sql) throws SQLException {
        Statement statement = connection.createStatement();
        System.out.println("Executing this command : " + sql);
        statement.executeUpdate(sql);
        connection.commit();
    }

    public ResultSet doSelect(String sql) {
        Statement statement = null;
        try {
            statement = connection.createStatement();
            System.out.println("Executing this command : " + sql);
            return statement.executeQuery(sql);
        } catch (SQLException e) {
            System.out.println("Cannot execute command" + sql + " : ");
            e.printStackTrace();
        }
        return null;
    }
}
