package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public final class DatabaseConnection {

    private static DatabaseConnection databaseConnection;
    Connection connection;

    private DatabaseConnection () {
        //private constructor
    }

    public static DatabaseConnection getInstance(){
        if (databaseConnection == null) {
            databaseConnection = new DatabaseConnection();
        }
        return databaseConnection;
    }

    public Connection getConnection() throws SQLException {
        if (connection == null) {
            String host = "jdbc:mysql://localhost:3306/sr03TD4";
            String username = "root";
            String password = "";
            connection = DriverManager.getConnection(host, username, password);
        }
        return connection;
    }


}
