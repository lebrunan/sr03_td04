-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 06 avr. 2020 à 16:38
-- Version du serveur :  5.7.19
-- Version de PHP :  5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `sr03td4`
--

-- --------------------------------------------------------

--
-- Structure de la table `forum`
--

DROP TABLE IF EXISTS `forum`;
CREATE TABLE IF NOT EXISTS `forum` (
  `ID_CREATOR` int(11) NOT NULL,
  `ID_FORUM` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(32) NOT NULL,
  `DESCRIPTION` text NOT NULL,
  PRIMARY KEY (`ID_FORUM`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `forum`
--

INSERT INTO `forum` (`ID_CREATOR`, `ID_FORUM`, `TITLE`, `DESCRIPTION`) VALUES
(0, 1, 'Forum de SR03', 'ce forum rassemble les conseils sur SR03');

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

DROP TABLE IF EXISTS `message`;
CREATE TABLE IF NOT EXISTS `message` (
  `ID_FORUM` int(11) NOT NULL,
  `CONTENT` text NOT NULL,
  `PUBLISH_DATE` date NOT NULL,
  `ID_MESSAGE` int(11) NOT NULL AUTO_INCREMENT,
  `ID_SENDER` int(11) NOT NULL,
  PRIMARY KEY (`ID_MESSAGE`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `message`
--

INSERT INTO `message` (`ID_FORUM`, `CONTENT`, `PUBLISH_DATE`, `ID_MESSAGE`, `ID_SENDER`) VALUES
(1, 'bonjour à tous !', '2020-04-06', 1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `NAME` varchar(32) NOT NULL,
  `FIRSTNAME` varchar(32) NOT NULL,
  `CREATION_DATE` date NOT NULL,
  `PASSWORD` varchar(32) NOT NULL,
  `EMAIL` varchar(60) NOT NULL,
  `IS_ADMIN` int(11) NOT NULL DEFAULT '0',
  `ID_USER` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID_USER`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`NAME`, `FIRSTNAME`, `CREATION_DATE`, `PASSWORD`, `EMAIL`, `IS_ADMIN`, `ID_USER`) VALUES
('Lebrun', 'Antoine', '2020-04-06', 'lebrunan', 'antoine.lebrun@etu.utc.fr', 1, 1),
('Roussel', 'Pierre', '2020-04-05', 'mot de passe', 'pierre.roussel@etu.utc.fr', 0, 2),
('Lebrun', 'Antoine', '2020-04-06', 'lebrunan', 'antoinelmh@hotmail.fr', 0, 3);

-- --------------------------------------------------------

--
-- Structure de la table `user_forum`
--

DROP TABLE IF EXISTS `user_forum`;
CREATE TABLE IF NOT EXISTS `user_forum` (
  `ID_USER` int(11) NOT NULL,
  `ID_FORUM` int(11) NOT NULL,
  `JOINING_DATE` date NOT NULL,
  PRIMARY KEY (`ID_USER`,`ID_FORUM`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user_forum`
--

INSERT INTO `user_forum` (`ID_USER`, `ID_FORUM`, `JOINING_DATE`) VALUES
(2, 1, '2020-04-06');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
